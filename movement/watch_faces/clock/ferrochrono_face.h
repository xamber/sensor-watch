/*
 * MIT License
 *
 * Copyright (c) 2024 electrochori
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef FERROTEMPUS_FACE_H_
#define FERROTEMPUS_FACE_H_

#include "movement.h"

/*
 * A DESCRIPTION OF YOUR WATCH FACE
 *
 * and a description of how use it
 *
 */

#define SHOW_REL_TIME true

typedef struct {
    uint8_t hour : 5;      // 0-23 | 31
    uint8_t minute : 6;    // 0-59 | 63
} SchedCell;

typedef struct {
    uint8_t stations;
    char name[6];
    char (*station)[3];         // pointer to an array of char[3]
} Line;

typedef struct {
    Line * line;
    uint8_t day_mask: 3;        // ___ -> 001 = workdays, 010 = saturdays, 100 = sundays+holidays
    uint8_t trains;
    SchedCell (*schedArr);         // pointer to an array of SchedCell
    /* SchedCell schedArr[]; */
} Schedule;

typedef enum {
    rw_select,
    rw_sched
} fc_mode;

typedef struct {
  // Anything you need to keep track of, put it here!
  uint8_t rw_line;
  uint8_t rw_station;
  bool    show_rel;
  fc_mode mode;
} ferrochrono_state_t;

void ferrochrono_face_setup(movement_settings_t *settings,
                            uint8_t watch_face_index, void **context_ptr);
void ferrochrono_face_activate(movement_settings_t *settings, void *context);
bool ferrochrono_face_loop(movement_event_t event,
                           movement_settings_t *settings, void *context);
void ferrochrono_face_resign(movement_settings_t *settings, void *context);

#define ARR_LEN(a) (sizeof(a) / sizeof(a[0]))

#define ferrochrono_face                                                       \
  ((const watch_face_t){                                                       \
      ferrochrono_face_setup,                                                  \
      ferrochrono_face_activate,                                               \
      ferrochrono_face_loop,                                                   \
      ferrochrono_face_resign,                                                 \
      NULL,                                                                    \
  })

#endif // FERROTEMPUS_FACE_H_

