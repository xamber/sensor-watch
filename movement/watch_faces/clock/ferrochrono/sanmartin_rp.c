#include "movement.h"
#include "ferrochrono_face.h"
#include "sanmartin_rp_scheds.c"

static char _retiro_pilar[][3] = {
    "RE",     // Retiro
    "PA",     // Palermo
    "VC",     // Villa Crespo
    "LP",     // La Paternal
    "VP",     // Villa del Parque
    "DE",     // Devoto
    "SP",     // Sáenz Peña
    "SL",     // Santos Lugares
    "CA",     // Caseros
    "EP",     // El Palomar
    "HU",     // Hurlingham
    "MO",     // W. C. Morris
    "BV",     // Bella Vista
    "MU",     // Muñiz
    "SM",     // San Miguel
    "JP",     // José C. Paz
    "SV",     // Sol y Verde
    "PD",     // Pte. Derqui
    "VA",     // Villa Astolﬁ
    "PI",     // Pilar
    "MA",     // Manzanares
    "DC",     // Dr. Cabred
};

static char _pilar_retiro[][3] = {
    "DC", "MA", "PI", "VA", "PD", "SV", "JP", "SM", "MU", "BV", "MO", "HU", "EP", "CA", "SL", "SP", "DE", "VP", "LP", "VC", "PA", "RE",
};

Line smrp_p_line = {
         /* "------", */
    .name = "SM P  ",
    .stations = ARR_LEN(_retiro_pilar),  // 22
    .station = _retiro_pilar,
};

Line smrp_r_line = {
         /* "------", */
    .name = "SM R ",
    .stations = ARR_LEN(_pilar_retiro),  // 22
    .station = _pilar_retiro,
};

/* San Martin Retiro-Pilar, to Retiro, monday-friday, 22 stations, 87 trains */
Schedule SMRP_Retiro_Wd_Schedule = {
    .line = &smrp_r_line,
    .day_mask = 0b001,  // monday-friday
    .trains = ARR_LEN(_pilar_retiro_wd) / ARR_LEN(_pilar_retiro),
    /* 22 stations * 87 trains, sched(hour, minutes) */
    .schedArr = _pilar_retiro_wd,
};

/* San Martin Retiro-Pilar, to Pilar, monday-friday, 22 stations, 86 trains */
Schedule SMRP_Pilar_Wd_Schedule = {
    .line = &smrp_p_line,
    .day_mask = 0b001,  // monday-friday
    .trains = ARR_LEN(_retiro_pilar_wd) / ARR_LEN(_retiro_pilar),
    /* 22 stations * 86 trains, sched(hour, minutes) */
    .schedArr = _retiro_pilar_wd,
};

/* San Martin Retiro-Pilar, to Retiro, saturday, 22 stations, 79 trains */
Schedule SMRP_Retiro_Sa_Schedule = {
    .line = &smrp_r_line,
    .day_mask = 0b010,  // saturday
    .trains = ARR_LEN(_pilar_retiro_sa) / ARR_LEN(_pilar_retiro),
    /* 22 stations * 79 trains, sched(hour, minutes) */
    .schedArr = _pilar_retiro_sa,
};

/* San Martin Retiro-Pilar, to Pilar, saturday, 22 stations, 77 trains */
Schedule SMRP_Pilar_Sa_Schedule = {
    .line = &smrp_p_line,
    .day_mask = 0b010,  // saturday
    .trains = ARR_LEN(_retiro_pilar_sa) / ARR_LEN(_retiro_pilar),
    /* 22 stations * 77 trains, sched(hour, minutes) */
    .schedArr = _retiro_pilar_sa,
};

/* San Martin Retiro-Pilar, to Retiro, sunday-holiday, 22 stations, 59 trains */
Schedule SMRP_Retiro_Ho_Schedule = {
    .line = &smrp_r_line,
    .day_mask = 0b100,  // sunday-holiday
    .trains = ARR_LEN(_pilar_retiro_ho) / ARR_LEN(_pilar_retiro),
    /* 22 stations * 59 trains, sched(hour, minutes) */
    .schedArr = _pilar_retiro_ho,
};

/* San Martin Retiro-Pilar, to Pilar, sunday-holiday, 22 stations, 58 trains */
Schedule SMRP_Pilar_Ho_Schedule = {
    .line = &smrp_p_line,
    .day_mask = 0b100,  // sunday-holiday
    .trains = ARR_LEN(_retiro_pilar_ho) / ARR_LEN(_retiro_pilar),
    /* 22 stations * 58 trains, sched(hour, minutes) */
    .schedArr = _retiro_pilar_ho,
};
