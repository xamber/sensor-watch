#include "movement.h"
#include "ferrochrono_face.h"
#include "mitre_rt_scheds.c"

static char _retiro_tigre[][3] = {
    "RE",     // Retiro
    "LT",     // L. de la Torre
    "BC",     // Belgrano C
    "NU",     // Núñez
    "RI",     // Rivadavia
    "VL",     // Vte. López
    "OL",     // Olivos
    "LL",     // La Lucila
    "MT",     // Martínez
    "AC",     // Acassuso
    "SI",     // San Isidro
    "BE",     // Beccar
    "VI",     // Victoria
    "VR",     // Virreyes
    "SF",     // San Fernando
    "CA",     // Carupá
    "TI",     // Tigre
};

static char _tigre_retiro[][3] = {
    "TI", "CA", "SF", "VR", "VI", "BE", "SI", "AC", "MT", "LL", "OL", "VL", "RI", "NU", "BC", "LT", "RE",
};

Line mrt_t_line = {
         /* "------", */
    .name = " MRTT",
    .stations = ARR_LEN(_retiro_tigre),  // 17
    .station = _retiro_tigre,
};

Line mrt_r_line = {
         /* "------", */
    .name = " MRTR",
    .stations = ARR_LEN(_tigre_retiro),  // 17
    .station = _tigre_retiro,
};

/* Mitre Retiro-Tigre, to Tigre, monday-friday, 17 stations, 70 trains */
Schedule MRT_Tigre_Wd_Schedule = {
    .line = &mrt_t_line,
    .day_mask = 0b001,  // monday-friday
    .trains = ARR_LEN(_retiro_tigre_wd) / ARR_LEN(_retiro_tigre),
    /* 17 stations * 70 trains, sched(hour, minutes) */
    .schedArr = _retiro_tigre_wd,
};

/* Mitre Retiro-Tigre, to Retiro, monday-friday, 17 stations, 70 trains */
Schedule MRT_Retiro_Wd_Schedule = {
    .line = &mrt_r_line,
    .day_mask = 0b001,  // monday-friday
    .trains = ARR_LEN(_tigre_retiro_wd) / ARR_LEN(_tigre_retiro),
    /* 17 stations * 70 trains, sched(hour, minutes) */
    .schedArr = _tigre_retiro_wd,
};

/* Mitre Retiro-Tigre, to Tigre, saturday-sunday-holiday, 17 stations, 46 trains */
Schedule MRT_Tigre_SaHo_Schedule = {
    .line = &mrt_t_line,
    .day_mask = 0b110,  // saturday-sunday-holiday
    .trains = ARR_LEN(_retiro_tigre_sa_ho) / ARR_LEN(_retiro_tigre),
    /* 17 stations * 46 trains, sched(hour, minutes) */
    .schedArr = _retiro_tigre_sa_ho,
};

/* Mitre Retiro-Tigre, to Retiro, saturday-sunday-holiday, 17 stations, 46 trains */
Schedule MRT_Retiro_SaHo_Schedule = {
    .line = &mrt_r_line,
    .day_mask = 0b110,  // saturday-sunday-holiday
    .trains = ARR_LEN(_tigre_retiro_sa_ho) / ARR_LEN(_tigre_retiro),
    /* 17 stations * 46 trains, sched(hour, minutes) */
    .schedArr = _tigre_retiro_sa_ho,
};
